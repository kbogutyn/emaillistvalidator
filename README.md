Zadanie
"Przygotuj program który będzie działał z wiersza poleceń np GIT Bash w oparciu o Symfony Console.

Program przyjmuje jeden parametr przy uruchamianiu, którym jest nazwa pliku CSV.

W załączniku plik, który zawiera listę adresów mailowych.
Zadaniem programu jest:

- wczytanie adresów z pliku CSV

- sprawdzenie czy adres email jest poprawny (spróbuj jak najlepiej zweryfikować czy jest taki adres email)

- utworzenie trzech plików wynikowych: jeden z wszystkimi poprawnymi adresami, drugimi z wszystkimi adresami odrzuconymi przy walidacji oraz plik txt z podsumowaniem walidacji 